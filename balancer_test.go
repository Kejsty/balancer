package main

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"
)

type testcustomer struct {
	customer
	startat int
	endat   int
}

//Workload custom workload to print custom jobids
func (c *testcustomer) Workload(ctx context.Context) chan int {
	workload := make(chan int)
	go func() {
		defer close(workload)

		load := c.startat
		for {
			select {
			case <-ctx.Done():
				return
			case workload <- load:
				if load -= 1; load == c.endat {
					return
				}
			}
		}
	}()
	return workload
}

// TheExpensiveFragileService is a service that we need to utilize as much as we can, since it's expensive to run,
// but on the other hand it's very fragile so we can't just flood it with thousands of requests per second.
type TheExpensiveFragileServiceTest struct {
	lock     sync.Mutex
	executed map[int]time.Time
}

// Process a single work chunk and return error if occurred.
func (t *TheExpensiveFragileServiceTest) Process(_ context.Context, jobid int) error {
	t.lock.Lock()
	if _, ok := t.executed[jobid]; ok {
		return fmt.Errorf("Alredy exists")
	}
	t.executed[jobid] = time.Now()
	t.lock.Unlock()
	// allways the same time
	time.Sleep(time.Duration(100) * time.Millisecond)
	return nil
}

func TestBalancer_ExecutesAll(t *testing.T) {
	service := TheExpensiveFragileServiceTest{
		executed: map[int]time.Time{},
	}
	b := NewBalancer(&service, 10)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	c1 := &customer{
		workload: 10,
		weight:   5,
	}
	b.Register(ctx, c1)

	<-ctx.Done()

	if len(service.executed) != 10 {
		t.Logf("Havent executed all jobs withing 1 sec")
		t.Fail()
	}
}

func TestBalancer_WOntExecuteMoreThanLimit(t *testing.T) {
	service := TheExpensiveFragileServiceTest{
		executed: map[int]time.Time{},
	}
	b := NewBalancer(&service, 10)

	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Millisecond)
	defer cancel()

	c1 := &customer{
		workload: 20,
		weight:   5,
	}
	b.Register(ctx, c1)

	<-ctx.Done()

	if len(service.executed) != 10 {
		t.Logf("Executed more than limit:  %d", len(service.executed))
		t.Fail()
	}
}

func TestBalancer_ExecutesAllMoreThanLimit(t *testing.T) {
	service := TheExpensiveFragileServiceTest{
		executed: map[int]time.Time{},
	}
	b := NewBalancer(&service, 10)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	c1 := &customer{
		workload: 20,
		weight:   5,
	}
	b.Register(ctx, c1)

	<-ctx.Done()

	if len(service.executed) != 20 {
		t.Logf("Havent executed all jobs withing 1 sec: done only %d", len(service.executed))
		t.Fail()
	}
}

func TestBalancer_TwoCustomersEqual(t *testing.T) {
	service := TheExpensiveFragileServiceTest{
		executed: map[int]time.Time{},
	}
	b := NewBalancer(&service, 10)

	//only 1 round should be successfull (not even finished)
	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Millisecond)
	defer cancel()

	c1 := customer{
		weight: 5,
	}
	tc1 := &testcustomer{
		startat: 20,
		endat:   0,
	}
	tc1.customer = c1

	c2 := customer{
		weight: 5,
	}
	tc2 := &testcustomer{
		startat: 40,
		endat:   20,
	}
	tc2.customer = c2
	b.Register(ctx, tc1)
	b.Register(ctx, tc2)

	<-ctx.Done()

	if len(service.executed) != 10 {
		t.Logf("Executed different number of jobs than expected %d", len(service.executed))
		t.Fail()
	}

	// its perferctly valid if c1 has more than 5 because of c2 was too slow while registering jobs
	//ideally c1 and c2 executed both 5 jobs, but we need to respect also c1 with more than 5 and c2 with less than 5

	c1count := 0
	c2count := 0

	for jobid := range service.executed {
		if jobid > 0 && jobid <= 20 {
			c1count++
		} else if jobid > 20 && jobid <= 40 {
			c2count++
		} else {
			t.Logf("Unexptected: found jobid %d", jobid)
			t.Fail()
		}
	}

	if c1count < 5 {
		t.Logf("C1 executed less jobs than expected based on his weight %d", c1count)
		t.Fail()
	}

	if c2count > 5 {
		t.Logf("C2 executed more jobs than expected based on his weight %d", c1count)
		t.Fail()
	}
}

func TestBalancer_TwoCustomersDiferentWeight(t *testing.T) {
	service := TheExpensiveFragileServiceTest{
		executed: map[int]time.Time{},
	}
	b := NewBalancer(&service, 10)

	//only 1 round should be successfull (not even finished)
	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Millisecond)
	defer cancel()

	c1 := customer{
		weight: 7,
	}
	tc1 := &testcustomer{
		startat: 20,
		endat:   0,
	}
	tc1.customer = c1

	c2 := customer{
		weight: 3,
	}
	tc2 := &testcustomer{
		startat: 40,
		endat:   20,
	}
	tc2.customer = c2
	b.Register(ctx, tc1)
	b.Register(ctx, tc2)

	<-ctx.Done()

	if len(service.executed) != 10 {
		t.Logf("Executed different number of jobs than expected %d", len(service.executed))
		t.Fail()
	}

	// its perferctly valid if c1 has more than 7 because of c2 was too slow while registering jobs
	// ideally, c1 and c2 executes 7 and 3 jobs (respectively), but it can vary.

	c1count := 0
	c2count := 0

	for jobid := range service.executed {
		if jobid > 0 && jobid <= 20 {
			c1count++
		} else if jobid > 20 && jobid <= 40 {
			c2count++
		} else {
			t.Logf("Unexptected: found jobid %d", jobid)
			t.Fail()
		}
	}

	if c1count < 7 { //cant have less than 7, since was registered first
		t.Logf("C1 executed less jobs than expected based on his weight %d", c1count)
		t.Fail()
	}

	if c2count > 3 { //cant have more than 3, due to weight
		t.Logf("C2 executed more jobs than expected based on his weight %d", c1count)
		t.Fail()
	}
}

func TestBalancer_TwoCustomersDiferentWeightMoreRounds(t *testing.T) {
	service := TheExpensiveFragileServiceTest{
		executed: map[int]time.Time{},
	}
	b := NewBalancer(&service, 10)

	//only 2 round should be successfull (2. not even finished)
	ctx, cancel := context.WithTimeout(context.Background(), 150*time.Millisecond)
	defer cancel()

	c1 := customer{
		weight: 7,
	}
	tc1 := &testcustomer{
		startat: 20,
		endat:   0,
	}
	tc1.customer = c1

	c2 := customer{
		weight: 3,
	}
	tc2 := &testcustomer{
		startat: 40,
		endat:   20,
	}

	tc2.customer = c2
	b.Register(ctx, tc1)
	b.Register(ctx, tc2)

	<-ctx.Done()

	if len(service.executed) != 20 {
		t.Logf("Executed different number of jobs than expected %d", len(service.executed))
		t.Fail()
	}

	c1count := 0
	c2count := 0

	for jobid := range service.executed {
		if jobid > 0 && jobid <= 20 {
			c1count++
		} else if jobid > 20 && jobid <= 40 {
			c2count++
		} else {
			t.Logf("Unexptected: found jobid %d", jobid)
			t.Fail()
		}
	}

	// its perferctly valid if c1 has more than 14 because of c2 was too slow while registering jobs,
	// but cant have more than 17, since in second round, c2 was surely competing also for resources
	// ideally, c1 and c2 executes 14 and 6 jobs (respectively), but it can vary.

	if c1count < 14 { //cant have less than 14, since was registered first
		t.Logf("C1 executed less jobs than expected based on his weight %d", c1count)
		t.Fail()
	}

	if c1count > 17 { //cant have more than 17, sice second round was fair
		t.Logf("C1 executed more jobs than expected based on his weight %d", c1count)
		t.Fail()
	}

	if c2count > 6 { //cant have more than 6, due to weight
		t.Logf("C2 executed more jobs than expected based on his weight %d", c1count)
		t.Fail()
	}

	if c2count < 3 { //cant have less than 3, sice second round was fair
		t.Logf("C2 executed less jobs than expected based on his weight %d", c1count)
		t.Fail()
	}
}

func TestBalancer_ThreeCustomersOneLeavesEarly(t *testing.T) {
	//based ~ on model example
	service := TheExpensiveFragileServiceTest{
		executed: map[int]time.Time{},
	}
	b := NewBalancer(&service, 10)

	//only 3 round should be successfull (3. not even finished)
	ctx, cancel := context.WithTimeout(context.Background(), 250*time.Millisecond)
	defer cancel()

	c1 := customer{
		weight: 2,
	}
	tc1 := &testcustomer{
		startat: 20,
		endat:   0,
	}
	tc1.customer = c1

	c2 := customer{
		weight: 2,
	}
	tc2 := &testcustomer{
		startat: 40,
		endat:   20,
	}

	tc2.customer = c2

	c3 := customer{
		weight: 6,
	}
	tc3 := &testcustomer{ //only 10 tasks
		startat: 50,
		endat:   40,
	}

	tc3.customer = c3
	b.Register(ctx, tc3)
	b.Register(ctx, tc1)
	b.Register(ctx, tc2)

	<-ctx.Done()

	if len(service.executed) != 30 {
		t.Logf("Executed different number of jobs than expected %d", len(service.executed))
		t.Fail()
	}

	c1count := 0
	c2count := 0
	c3count := 0

	for jobid := range service.executed {
		if jobid > 0 && jobid <= 20 {
			c1count++
		} else if jobid > 20 && jobid <= 40 {
			c2count++
		} else if jobid > 40 && jobid <= 50 {
			c3count++
		} else {
			t.Logf("Unexptected: found jobid %d", jobid)
			t.Fail()
		}
	}

	//c3 must have executed all
	if c3count != 10 {
		t.Logf("C3 havent executed all, only %d", c3count)
		t.Fail()
	}

	// c1 should have executed between 10-12 based on the start)
	if !(c1count >= 10 && c1count <= 12) {
		t.Logf("C1 havent executed what expected, only %d", c1count)
		t.Fail()
	}

	// c2 should have executed between 8-10 based on the start)
	if !(c2count >= 8 && c2count <= 10) {
		t.Logf("C1 havent executed what expected,, only %d", c2count)
		t.Fail()
	}
}
