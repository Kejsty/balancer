package main

import (
	"context"
	"math"
	"sync"

	"github.com/sirupsen/logrus"
)

// Balancer should utilize the expensive service as much as it can while making sure it does not overflow it with
// work chunks. There is a hard limit for max parallel chunks that can't ever be crossed since there's a SLO defined
// and we don't want to make the expensive service people angry.
//
// There can be arbitrary number of Customers registered at any given time, each with it's own weight. Make sure to
// correctly assign processing capacity to a customer based on other customers currently in process.
//
// To give an example of this, imagine there's a maximum number of work chunks set to 100 and there are two customers
// registered, both with the same priority. When they are both served in parallel, each of them gets to send
// 50 chunks at the same time.
//
// In the same scenario, if there were two customers with priority 1 and one customer with priority 2, the first
// two would be allowed to send 25 chunks and the other one would send 50. It's likely that the one sending 50 would
// be served faster, finishing the work early, meaning that it would no longer be necessary that those first two
// customers only send 25 each but can and should use the remaining capacity and send 50 again.
type Balancer struct {
	tefs TheExpensiveFragileServiceI
	max  int

	cond        *sync.Cond    //to notify customers they can try again
	usedOverall int           //how much is used
	usedCount   map[*task]int // how many spots are used by customers

	currentWeightSum int //what is the overall sum of all registered customers
	lock             sync.RWMutex
}

// NewBalancer takes a reference to the main service and a maximum number of work chunks that can be fed through it
// in parallel.
func NewBalancer(tefs TheExpensiveFragileServiceI, max int) *Balancer {
	logrus.Info("NewBalancer: ", max)
	b := &Balancer{tefs: tefs, max: max, usedCount: map[*task]int{}}
	cond := sync.NewCond(&b.lock)
	b.cond = cond
	return b
}

// Register a customer to the balancer and start processing it's work chunks through the service.
func (b *Balancer) Register(ctx context.Context, c Customer) {
	t := b.register(c)

	go func() {
		jobs := c.Workload(ctx)

		for job := range jobs {

			b.cond.L.Lock() //lock so you can attempt to take spot for a job
			for {
				if b.tryTakeSpotAlreadyLocked(t) {
					go b.process(ctx, t, job)
					break //this job is done
				} else {
					b.cond.Wait() //unlocks and blocks
				}
			}
			b.cond.L.Unlock()
		}

		b.unregister(t) //customers tasks are done
	}()
}

func (b *Balancer) register(c Customer) *task {
	b.lock.Lock()
	defer b.lock.Unlock()

	t := &task{
		weight: c.Weight(),
	}

	b.usedCount[t] = 0
	b.currentWeightSum += c.Weight() //should change ratio
	logrus.Info("registered: ", c, "as task: ", t)
	return t
}

func (b *Balancer) unregister(t *task) {
	b.lock.Lock()
	defer b.lock.Unlock()

	b.currentWeightSum -= t.weight //should change ratio
	logrus.Info("unregistered (done): ", *t)
	b.cond.Broadcast() //let waiting nodes now, they can try again - weights may have changed
}

func (b *Balancer) spotFreed(t *task) {
	b.lock.Lock()
	defer b.lock.Unlock()
	b.usedCount[t] -= 1
	b.usedOverall -= 1

	if b.usedCount[t] == 0 {
		delete(b.usedCount, t) //so we wont hold it forever
	}

	b.cond.Broadcast() //let waiting nodes now, they can try again
}

func (b *Balancer) tryTakeSpotAlreadyLocked(t *task) bool {

	if b.usedOverall >= b.max {
		return false //already taken by somebody else
	}

	maxSpots := math.Round(float64(b.max*t.weight) / float64(b.currentWeightSum))
	if b.usedCount[t] >= int(maxSpots) {
		return false //already on limit
	}

	//success, take job
	b.usedCount[t] += 1
	b.usedOverall += 1
	return true
}

func (b *Balancer) process(ctx context.Context, c *task, job int) {
	b.tefs.Process(ctx, job)
	b.spotFreed(c) //free spot after job is done
}

type task struct {
	weight int
}
